import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import ProductosServicios from "../../servicios/ProductosServicios";
const FormProductos = () => {

    const navigateTo = useNavigate()
    const { id } = useParams();

    const [nombreproducto, setNombreproducto] = useState("");
    const [nombrecategoria, setNombrecategoria] = useState("");
    const [nombreproveedor, setNombreproveedor] = useState("");
    const [cantidaddisponible, setCantidaddisponible] = useState("");
    const [preciocompra, setPreciocompra] = useState("");
    const [precioventa, setPrecioventa] = useState("");
    const [fecharegistroproducto, setFecharegistroproducto] = useState("");
    const [imagen, setImagen] = useState("");
    const [disp, setDisp] = useState(false);

    const [mensaje, setMensaje] = useState("");
    const [titulo, setTitulo] = useState("");

    const cargarProducto = async () => {
        try {
            if (id != null) {
                const respuesta = await ProductosServicios.buscarProducto(id);
                if (respuesta.data != null) {
                    console.log(respuesta.data);
                    setNombreproducto(respuesta.data.nombre_producto);
                    setNombrecategoria(respuesta.data.categoria);
                    setNombreproveedor(respuesta.data.proveedor);
                    setCantidaddisponible(respuesta.data.cantidad_disponible);
                    setPreciocompra(respuesta.data.precio_compra);
                    setPrecioventa(respuesta.data.precio_venta);
                    setFecharegistroproducto(respuesta.data.fecha_registro_producto);
                    setImagen(respuesta.data.imagen);
                    setDisp(respuesta.data.disp);
                }
                setTitulo("Edición");
            }
            else {
                setTitulo("Registro");
            }
        } catch (error) {
            console.log("Ocurrió un error");
        }
    }

    const guardarProducto = async (event) => {
        event.preventDefault();
        try {
            const producto = {
                nombre_producto: nombreproducto,
                categoria: nombrecategoria,
                proveedor: nombreproveedor,
                cantidad_disponible: cantidaddisponible,
                precio_compra: preciocompra,
                precio_venta: precioventa,
                fecha_registro_producto: fecharegistroproducto,
                imagen: imagen,
                disp: disp
            }
            console.log(producto);
            if (id == null) {
                await ProductosServicios.guardarProducto(producto);
                navigateTo("/productos");
            }
            else {
                await ProductosServicios.modificarProducto(id, producto);
                navigateTo("/productos");
            }
        } catch (error) {
            setMensaje("Ocurrió un errór :(")
        }
    }



    const cancelar = () => {
        if (id != null) {
            navigateTo("/productos");
        }
        else {
            navigateTo("/");
        }

    }
    useEffect(() => {
        cargarProducto();
    }, [])

    const cambiarNombrecategoria = (event) => {
        setNombrecategoria(event.target.value);
    }
    const cambiarNombreproducto = (event) => {
        setNombreproducto(event.target.value);
    }
    const cambiarNombreproveedor = (event) => {
        setNombreproveedor(event.target.value);
    }
    const cambiarCantidaddisponible = (event) => {
        setCantidaddisponible(event.target.value);
    }
    const cambiarPreciocompra = (event) => {
        setPreciocompra(event.target.value);
    }
    const cambiarPrecioventa = (event) => {
        setPrecioventa(event.target.value);
    }
    const cambiarFecharegistroproducto = (event) => {
        setFecharegistroproducto(event.target.value);
    }
    const cambiarImagen = (event) => {
        setImagen(event.target.value);
    }
    
    return (
        <div className="container mt-3">
            <h3>{titulo} de productos</h3>
            <form className="container" action="">
                <div className="row">
                    <div className="col-4">
                        <label className="form-control-sm" htmlFor="documento">Nombre del producto</label>
                        <input className="form-control form-control-sm" type="text" onChange={cambiarNombreproducto} value={nombreproducto} name="nombreproducto" id="nombreproducto" placeholder="Ingrese el nombre del producto" required />
                    </div>
                    <div className="col-4">
                        <label className="form-control-sm" htmlFor="nombrecategoria">Nombre de la categoria</label>
                        <select className="form-select form-select-sm" type="select" onChange={cambiarNombrecategoria} vvalue={nombrecategoria} name="nonmbrecategoria" id="nombrecategoria" placeholder="Ingrese el nombre de la categoria" required >
                            <option value="NA" type="text">Seleccione la categoria</option>
                            <option value="Galletería">Galletería</option>
                            <option value="Galletería">Chocolatería</option>
                            <option value="Confitería">Confitería</option>
                            <option value="Saludables">Saludables</option>
                            <option value="Snacks">Snacks</option>
                            <option value="Packs">Packs</option>
                        </select>
                    </div>
                    <div className="col-4">
                        <label className="form-control-sm" htmlFor="documento">Nombre del proveedor</label>
                        <input className="form-control form-control-sm" type="text" onChange={cambiarNombreproveedor} value={nombreproveedor} name="nombreproveedor" id="nombreproveedor" placeholder="Ingrese el nombre del proveedor" required />
                    </div>
                    <div className="col-4">
                        <label className="form-control-sm" htmlFor="documento">Cantidad disponible</label>
                        <input className="form-control form-control-sm" type="number" onChange={cambiarCantidaddisponible} value={cantidaddisponible} name="cantidaddisponible" id="cantidaddisponible" placeholder="Ingrese la cantidad" required />
                    </div>
                    <div className="col-4">
                        <label className="form-control-sm" htmlFor="documento">Precio de compra</label>
                        <input className="form-control form-control-sm" type="number" onChange={cambiarPreciocompra} value={preciocompra} name="preciocompra" id="preciocompra" placeholder="Ingrese la cantidad" required />
                    </div>
                    <div className="col-4">
                        <label className="form-control-sm" htmlFor="documento">Precio de venta</label>
                        <input className="form-control form-control-sm" type="number" onChange={cambiarPrecioventa} value={precioventa} name="precioventa" id="precioventa" placeholder="Ingrese la cantidad" required />
                    </div>
                    <div className="col-4">
                        <label className="form-control-sm" htmlFor="documento">Fecha de registro</label>
                        <input className="form-control form-control-sm" type="text" onChange={cambiarFecharegistroproducto} value={fecharegistroproducto} name="fecharegistroproducto" id="fecharegistroproducto" placeholder="Ingrese el nombre del proveedor" required />
                    </div>
                    <div className="col-4">
                        <label className="form-control-sm" htmlFor="documento">Imagen</label>
                        <input className="form-control form-control-sm" type="text" onChange={cambiarImagen} value={imagen} name="imagen" id="imagen" placeholder="Ingrese la URL de la imagen" required />
                    </div>

                </div>
                <div className="mt-3">
                    <button onClick={guardarProducto} className="btn btn-warning me-2">Guardar</button>
                    <button onClick={cancelar} href="/productos" className="btn btn-secondary">Cancelar</button>
                    <div id="mensaje">{mensaje}</div>
                </div>
            </form>
        </div>
    )
}

export default FormProductos;