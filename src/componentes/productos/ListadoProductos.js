import { useState, useEffect } from "react";
import Estados from "../../enums/Estados"
import ProductosServicios from "../../servicios/ProductosServicios";

const ListadoProductos = () => {

    const [listadoProductos, setListadoProductos] = useState([]);
    const [estado, setEstado] = useState(Estados.CARGANDO);
    const [criterio, setCriterio] = useState("");
    const [idBorrar, setIdBorrar] = useState("");
    const [nombreBorrar, setNombreBorrar] = useState("");

    const cargarProductos = async () => {
        try {
            const respuesta = await ProductosServicios.listarProductos();
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoProductos(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }
    }

    const buscarProductos = async (event) => {
        event.preventDefault()
        try {
            const respuesta = await ProductosServicios.buscarProductos(criterio);
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoProductos(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }
    }

    const confirmarBorrado = (id, nombre) => {
        setIdBorrar(id);
        setNombreBorrar(nombre);
    }

    const cambiarCriterio = (event) => {
        setCriterio(event.target.value);
    }

    const borrarProducto = async () => {
        try {
            await ProductosServicios.borrarProducto(idBorrar);
            cargarProductos();
        } catch (error) {

        }
    }

    useEffect(() => {
        cargarProductos();
    }, [])

    return (
        <div className="container">
            <h3 className="mt-3">Lista de productos</h3>
            <form action="">
                <input type="text" value={criterio} onChange={cambiarCriterio} id="criterio" name="criterio" />
                <button id="buscar" name="buscar" onClick={buscarProductos} >Buscar</button>
            </form>

            <table className="table table-sm">
                <thead>
                    <tr>
                        <th>Nombre de producto</th>
                        <th>Nombre de categoria</th>
                        <th>Cantidad disponible</th>
                        <th>Precio de compra</th>
                        <th>Precio de venta</th>
                        <th>Proveedor</th>
                        <th>Fecha de registro</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {estado === Estados.CARGANDO ?
                        (<tr><td>
                            <div className="spinner-border text-warning" role="status">
                                <span className="sr-only"></span>
                            </div>
                        </td></tr>)
                        :
                        estado === Estados.ERROR ?
                            (<tr><td>No se ha encontrado información</td></tr>)
                            :
                            estado === Estados.VACIO ?
                                (<tr><td>No hay datos</td></tr>)
                                :
                                listadoProductos.map((producto) => (
                                    <tr key={producto._id}>
                                        <td>{producto.nombre_producto}</td>
                                        <td>{producto.categoria}</td>
                                        <td>{producto.cantidad_disponible}</td>
                                        <td>{producto.precio_compra}</td>
                                        <td>{producto.precio_venta}</td>
                                        <td>{producto.proveedor}</td>
                                        <td>{producto.fecha_registro_producto}</td>
                                        <td>
                                            <a href={"/productos/form/" + producto._id} className="btn btn-sm btn-warning me-2">Editar</a>
                                            <button onClick={() => {confirmarBorrado(producto._id, producto.nombre_producto) }} className="btn btn-sm btn-secondary me-2" data-bs-toggle="modal" data-bs-target="#modalBorrado">Eliminar</button>
                                        </td>
                                    </tr>
                                ))
                    }
                </tbody>
            </table>
            <a href="/productos/form" type="button" className="btn btn-dark">Crear Producto</a>

            <div className="modal fade" id="modalBorrado" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="staticBackdropLabel">Borrado de producto</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            ¿Está seguro de borrar el producto {nombreBorrar}?
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-warning" data-bs-dismiss="modal">Cancelar</button>
                            <button type="button" onClick={borrarProducto} className="btn btn-secondary" data-bs-dismiss="modal">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ListadoProductos;