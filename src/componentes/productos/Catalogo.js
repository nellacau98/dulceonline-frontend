import { useEffect } from "react";
import { useState } from "react";
import { useLocation } from "react-router-dom";
import Estados from "../../enums/Estados";
import ProductosServicios from "../../servicios/ProductosServicios";
import TarjetaProducto from "./TarjetaProducto";

const Catalogo = () => {
	const query = useLocation();
	const [estado, setEstado] = useState(Estados.LISTO);
	const [productos, setProductos] = useState([]);
	const [criterio, setCriterio] = useState("");

	const cargarProductos = async (categoria="") => {
		setEstado(Estados.CARGANDO);
		try {
			let resultado;
			if (criterio !== "") {
				resultado = await ProductosServicios.buscarProductos(criterio);
			}
			else if (categoria !== "") {
				resultado = await ProductosServicios.buscarProductos(categoria);
			}
			else {
				resultado = await ProductosServicios.listarProductos();
			}
			if (resultado.data.length === 0) {
				setEstado(Estados.VACIO);
			}
			else {
				setProductos(resultado.data);
				console.log(resultado.data);
				setEstado(Estados.LISTO);
			}
		} catch (error) {
			setEstado(Estados.ERROR);
			console.log(error);
		}
	}

	const cambiarCriterio = (event) => {
		setCriterio(event.target.value);
	}

	const buscarProductos = (event) => {
		event.preventDefault();
		cargarProductos();
	}

	useEffect(() => {
		const catBuscar = query.search.replace("?q=", "");
		cargarProductos(catBuscar);
	}, [])

	return (
		<main id="catalogo" className="container-fluid">
			<form className="mb-3">
				<div className="d-flex">
					<label className="form-label me-2" htmlFor="criterio">Buscar producto</label> 
					<input className="form-control form-control-sm" style={{maxWidth:"300px"}} onChange={cambiarCriterio} value={criterio} type="text" id="criterio" name="criterio" />
					<button onClick={buscarProductos} className="btn btn-sm btn-dark"><i className="bi bi-search" /></button>
				</div>
			</form>
			<div className="row mb-2">
				{
					estado===Estados.CARGANDO ? (<div>Cargando...</div>) 
				: 
					estado===Estados.VACIO ? (<div>No hay datos</div>) 
				:
					productos.map((producto) => (
						(<TarjetaProducto key={producto._id} producto={producto}/>)
					))
				}
			</div>
		</main>
	);
}

export default Catalogo;