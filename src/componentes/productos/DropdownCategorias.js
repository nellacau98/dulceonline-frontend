const DropdownCategorias = ({ categoria }) => {
    return (
        <div className="col-4">
            <label className="form-control-sm" htmlFor="nombrecategoria">Nombre de la categoria</label>
            <select className="form-select form-select-sm" type="select" name="nonmbrecategoria" id="nombrecategoria" placeholder="Ingrese el nombre de la categoria" required >
                <option>Seleccione la categoria</option>
            </select>
        </div>

    )
}

export default DropdownCategorias;