const TarjetaProducto = ({ producto }) => {
    return (
        <div className="col-3 mb-2">
            <div className="card border-warning">
                <div className="card-body row mx-auto">
                    <img class="card-img-top" src={producto.imagen} alt="producto" />
                    <h5 className="card-title">{producto.nombre_producto}</h5>
                    <h4 className="card-text">$ {producto.precio_venta}</h4>
                    <p className="card-text">Categoria: {producto.categoria}</p>
                    <p className="card-text">Proveedor: {producto.proveedor}</p>
                    <button className="btn btn-warning mx-auto">Añadir al carrito</button>
                </div>
            </div>
        </div>
        
    )
}

export default TarjetaProducto;