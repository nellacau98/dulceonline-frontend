import { useState, useEffect } from "react";
import Estados from "../../enums/Estados";
import PedidosServicios from "../../servicios/PedidosServicios";

const ListadoPedidos = () => {

    const [ listadoPedidos, setListadoPedidos ] = useState([]);
    const [ estado, setEstado ] = useState(Estados.CARGANDO);
    const [ criterio, setCriterio ] = useState("");
    const [ idBorrar, setIdBorrar ] = useState("");
    const [ pedidoBorrar, setPedidoBorrar ] = useState("");


    const cargarPedidos = async () => {
        try {
            const respuesta = await PedidosServicios.listarPedidos();
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoPedidos(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }
    }

    const buscarPedidos = async (event) => {
        event.preventDefault();
        try {
            const respuesta = await PedidosServicios.buscarPedidos(criterio);
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoPedidos(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }
    }

    const confirmarBorrado = (id, pedido) => {
        setIdBorrar(id);
        setPedidoBorrar(pedido);
    }

    const borrarPedido = async () => {
        try {
            await PedidosServicios.borrarPedido(idBorrar);
            cargarPedidos();
        } catch (error) {
            
        }
    }
    

    useEffect(() => {
        cargarPedidos();
    }, [])

    const cambiarCriterio = (event) => {
        setCriterio(event.target.value);
    }

    return (
        <div className="container">
            <h3 className="mt-2">Lista de Pedidos</h3>
            <form action="">
                <input  type="text" value={criterio} onChange={cambiarCriterio} id="criterio" name="criterio"/>
                <button className ="btn btn-dark mx-1" id="buscar" name="buscar" onClick={buscarPedidos}>Buscar</button>
            </form>
            <table className="table table-sm"> 
                <thead>
                    <tr>
                        <th>Pedido</th>
                        <th>Precio de Venta</th>
                        <th>Precio de Envio</th>
                        <th>Total Pedido</th>
                        <th>Fecha Pedido</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {estado === Estados.CARGANDO ? 
                            (<tr>
                                <td>
                                <div className="spinner-border text-warning" role="status">
                                    <span className="sr-only"></span>
                                </div>
                            </td>
                            </tr>) 
                        :
                        estado === Estados.ERROR ? 
                            (<tr><td>Ocurrió un error, intente más tarde</td></tr>) 
                        :
                        estado === Estados.VACIO ? 
                            (<tr><td>No hay datos</td></tr>) 
                        : 
                        listadoPedidos.map((pedido) => (
                            <tr key={pedido._id}>
                                <td>{ pedido.pedido }</td>
                                <td>{ pedido.precio_venta }</td>
                                <td>{ pedido.precio_envio }</td>
                                <td>{ pedido.total_pedido }</td>
                                <td>{ pedido.fecha_pedido }</td>
                                <td>
                                <a className="btn btn-sm btn-warning me-2" href={"/pedidos/form/" + pedido._id}>Editar</a>
                                <button onClick={() => {confirmarBorrado(pedido._id, pedido.pedido)}} className="btn btn-sm btn-secondary me-2" data-bs-toggle="modal" data-bs-target="#modalBorrado">Eliminar</button>
                                </td>
                            </tr>
                        )) 
                    }
                </tbody>
            </table>
            <a href="/pedidos/form" type="button" className="btn btn-dark">Crear pedido</a>
            <div className="modal fade" id="modalBorrado" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="staticBackdropLabel">Borrado de pedido</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            Está seguro de borrar el pedido {pedidoBorrar}?
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-warning" data-bs-dismiss="modal">Cancelar</button>
                            <button type="button" onClick={borrarPedido} className="btn btn-secondary" data-bs-dismiss="modal">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ListadoPedidos;
