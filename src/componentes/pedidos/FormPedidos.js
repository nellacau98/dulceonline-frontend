import { useEffect } from "react";
import { useState } from "react";
import PedidosServicios from "../../servicios/PedidosServicios";
import { useNavigate, useParams } from "react-router-dom";


const FormPedidos = () => {

    const { id } = useParams();
    const navigateTo = useNavigate()

    const [pedido, setPedido] = useState("");
    const [precio_venta, setPrecioventa] = useState("");
    const [precio_envio, setPrecioenvio] = useState("");
    const [total_pedido, setTotalpedido] = useState("");
    const [fecha_pedido, setFechapedido] = useState("");
    const [titulo, setTitulo] = useState("");


    const guardarPedido = async (event) => {
        event.preventDefault();

        try {
            const pedidos = {
                pedido: pedido,
                precio_venta: precio_venta,
                precio_envio: precio_envio,
                total_pedido: total_pedido,
                fecha_pedido: fecha_pedido,

            }
            console.log(pedido);
            if (id == null) {
                await PedidosServicios.guardarPedido(pedidos);
                navigateTo("/pedidos");
            } else {
                await PedidosServicios.modificarPedido(id, pedidos);
                navigateTo("/pedidos");
            }

        } catch (error) {
            console.log("Ocurrió un error");
        }

    }


    const cargarPedido = async () => {
        try {
            if (id != null) {
                const respuesta = await PedidosServicios.buscarPedido(id);
                if (respuesta.data != null) {
                    console.log(respuesta.data);
                    setPedido(respuesta.data.pedido);
                    setPrecioventa(respuesta.data.precio_venta);
                    setPrecioenvio(respuesta.data.precio_envio);
                    setTotalpedido(respuesta.data.total_pedido);
                    setFechapedido(respuesta.data.fecha_pedido);

                }
                setTitulo("Edición");
            }
            else {
                setTitulo("Registro");
            }
        } catch (error) {
            console.log("Ocurrió un error");
        }
    }



    useEffect(() => {
        cargarPedido();
    }, [])

    const cambiarPedido = (event) => {
        setPedido(event.target.value);
    }

    const cambiarPrecioventa = (event) => {
        setPrecioventa(event.target.value);
    }

    const cambiarPrecioenvio = (event) => {
        setPrecioenvio(event.target.value);
    }

    const cambiarTotalpedido = (event) => {
        setTotalpedido(event.target.value);
    }
    const cambiarFechapedido = (event) => {
        setFechapedido(event.target.value);
    }

    return (
        <div className="container mt-3">
            <h3>{titulo} de Pedidos</h3>
            <form className="container" action="">
                <div className="row">
                    <div className="col-4">
                        <label htmlFor="pedido">Pedido *</label>
                            <input className="form-control" type="text" onChange={cambiarPedido} value={pedido} name="pedido" id="pedido" required />
                    </div>
                    <div className="col-4">
                        <label htmlFor="precio_venta">Precio de Venta *</label>
                        <input className="form-control" type="number" onChange={cambiarPrecioventa} value={precio_venta} name="precio_venta" id="precio_venta" required />
                    </div>
                    <div className="col-4">
                        <label htmlFor="precio_envio">Precio de Envio *</label>
                        <input className="form-control" type="number" onChange={cambiarPrecioenvio} value={precio_envio} name="precio_envio" id="precio_envio" required />
                    </div>
                    <div className="col-4">
                        <label htmlFor="total_pedido">Total del Pedido *</label>
                        <input className="form-control" type="number" onChange={cambiarTotalpedido} value={total_pedido} name="total_pedido" id="total_pedido" required />
                    </div>
                    <div className="col-4">
                        <label htmlFor="fecha_pedido">Fecha del Pedido *</label>
                        <input className="form-control" type="text" onChange={cambiarFechapedido} value={fecha_pedido} name="fecha_pedido" id="fecha_pedido" required />
                    </div>
                    <div className="row">

                    </div>
                </div>
            </form><br></br>
            <div>
                <button onClick={guardarPedido} className="btn btn-warning me-2">Guardar</button>
                <a href="/pedidos" type="button" className="btn btn-secondary">Cancelar</a>
            </div>
        </div>
    )
}

export default FormPedidos;