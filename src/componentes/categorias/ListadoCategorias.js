import { useState, useEffect } from "react";
import Estados from "../../enums/Estados"
import CategoriasServicios from "../../servicios/CategoriasServicios";

const ListadoCategorias = () => {

    const [listadoCategorias, setListadoCategorias] = useState([]);
    const [estado, setEstado] = useState(Estados.CARGANDO);
    const [criterio, setCriterio] = useState("");
    const [idBorrar, setIdBorrar] = useState("");
    const [nombreBorrar, setNombreBorrar] = useState("");

    const cargarCategorias = async () => {
        try {
            const respuesta = await CategoriasServicios.listarCategorias();
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoCategorias(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }
    }

    const buscarCategorias = async (event) => {
        event.preventDefault()
        try {
            const respuesta = await CategoriasServicios.buscarCategorias(criterio);
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoCategorias(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }
    }

    const confirmarBorrado = (id, nombre) => {
        setIdBorrar(id);
        setNombreBorrar(nombre);
    }

    const cambiarCriterio = (event) => {
        setCriterio(event.target.value);
    }

    const borrarCategoria = async () => {
        try {
            await CategoriasServicios.borrarCategoria(idBorrar);
            cargarCategorias();
        } catch (error) {
        }
    }

    useEffect(() => {
        cargarCategorias();
    }, [])

    return (
        <div className="container">
            <h3 className="mt-3">Lista de categoría</h3>
            <form action="">
                <input type="text" value={criterio} onChange={cambiarCriterio} id="criterio" name="criterio" />
                <button id="buscar" name="buscar" onClick={buscarCategorias} >Buscar</button>
            </form>

            <table className="table table-sm">
                <thead>
                    <tr>
                        <th>Nombre de categoria</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {estado === Estados.CARGANDO ?
                        (<tr><td>
                            <div className="spinner-border text-warning" role="status">
                            <span className="sr-only"></span>
                            </div>
                        </td></tr>)
                        :
                        estado === Estados.ERROR ?
                            (<tr><td>No se ha encontrado información</td></tr>)
                            :
                            estado === Estados.VACIO ?
                                (<tr><td>No hay datos</td></tr>)
                                :
                                listadoCategorias.map((categoria) => (
                                    <tr key={categoria._id}>
                                        <td>{categoria.nombre_categoria}</td>
                                        <td>
                                            <a href={"/categorias/form/" + categoria._id} className="btn btn-sm btn-warning me-2">Editar</a>
                                            <button onClick={() => {confirmarBorrado(categoria._id, categoria.nombre_producto) }} className="btn btn-sm btn-secondary me-2" data-bs-toggle="modal" data-bs-target="#modalBorrado">Eliminar</button>
                                        </td>
                                    </tr>
                                ))
                    }
                </tbody>
            </table>
            <a href="/categorias/form" type="button" className="btn btn-dark">Crear Categoria</a>

            <div className="modal fade" id="modalBorrado" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="staticBackdropLabel">Borrado de categoria</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            ¿Está seguro de borrar el producto {nombreBorrar}?
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-warning" data-bs-dismiss="modal">Cancelar</button>
                            <button type="button" onClick={borrarCategoria} className="btn btn-secondary" data-bs-dismiss="modal">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ListadoCategorias;