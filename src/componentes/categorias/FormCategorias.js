import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import CategoriasServicios from "../../servicios/CategoriasServicios";

const FormCategorias = () => {

    const { id } = useParams();
    const navigateTo = useNavigate()

    const [nombrecategoria, setNombrecategoria] = useState("");
    const [nombreproducto, setNombreproducto] = useState("");
    const [cantidadproveedores, setCantidadproveedores] = useState("");
    const [mensaje, setMensaje] = useState("");
    const [titulo, setTitulo] = useState("");


    const guardarCategoria = async (event) => {
        event.preventDefault();
        try {
            const categoria = {
                nombre_categoria: nombrecategoria,
                nombre_producto: nombreproducto,
                cantidad_proveedores: cantidadproveedores,
            }
            console.log(categoria);
            if (id == null) {
                await CategoriasServicios.guardarCategoria(categoria);
                navigateTo("/categorias");
            }
            else {
                await CategoriasServicios.modificarCategoria(id, categoria);
                navigateTo("/categorias");
            }
        } catch (error) {
            setMensaje("Ocurrió un error")
        }
    }

    const cargarCategoria = async () => {
        try {
            if (id != null) {
                const respuesta = await CategoriasServicios.buscarCategoria(id);
                if (respuesta.data != null) {
                    console.log(respuesta.data);
                    setNombrecategoria(respuesta.data.nombre_categoria);
                    setNombreproducto(respuesta.data.nombre_producto);
                    setCantidadproveedores(respuesta.data.cantidad_proveedores);
                }
                setTitulo("Edición");
            }
            else {
                setTitulo("Registro");
            }
        } catch (error) {
            console.log("Ocurrió un error");
        }
    }

    const cancelar = () => {
        if (id != null) {
            navigateTo("/categorias");
        }
        else {
            navigateTo("/");
        }
    }

    useEffect(() => {
        cargarCategoria();
    }, [])

    const cambiarNombrecategoria = (event) => {
        setNombrecategoria(event.target.value);
    }


    return (
        <div className="container mt-3">
            <h3>{titulo} de categorias</h3>
            <form className="container" action="">
                <div className="row">
                <div className="col-4">
                        <label className="form-control-sm" htmlFor="nombrecategoria">Nombre de la categoria</label>
                        <input className="form-control form-control-sm" type="text" onChange={cambiarNombrecategoria} value={nombrecategoria} name="nonmbrecategoria" id="nombrecategoria" placeholder="Ingrese el nombre de la categoria" required />
                    </div>

                </div>
                <div className="mt-3">
                    <button onClick={guardarCategoria} className="btn btn-warning me-2">Guardar</button>
                    <button onClick={cancelar} href="/categorias" className="btn btn-secondary">Cancelar</button>
                    <div id="mensaje">{mensaje}</div>
                </div>
            </form>
        </div>
    )
}

export default FormCategorias;