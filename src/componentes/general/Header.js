import { useContext } from "react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import EstadoLogin from "../../enums/EstadoLogin";
import { ContextoUsuario } from "./ContextoUsuario";
import logo from "../../assets/logo-sm.png";

const Header = () => {
    const navigateTo = useNavigate();
    const { usuario, setUsuario } = useContext(ContextoUsuario);

    const revisarSesion = () => {
        if (sessionStorage.getItem("estadologin") != null) {
            const usuarioSesion = {
                nombres: sessionStorage.getItem("nombres"),
                estadologin: parseInt(sessionStorage.getItem("estadologin"))
            }
            console.log(usuarioSesion);
            setUsuario(usuarioSesion);
        }
        else {
            setUsuario({ nombres: "", estadologin: EstadoLogin.NO_LOGUEADO });
        }
    }

    const cerrarSesion = () => {
        sessionStorage.clear();
        revisarSesion();
        navigateTo("/");
    }

    useEffect(() => {
        revisarSesion();
    }, [])

    return (
        <nav className="navbar navbar-link-warning navbar-expand-lg bg-warning mb-3">
            <div className="container-fluid">
                <a className="navbar-brand" href="/">
                    <img src="https://cdn-icons-png.flaticon.com/512/1888/1888859.png " alt="logo" width="50" href="/"/>
                </a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#menu" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                {
                    usuario.estadologin === EstadoLogin.NO_LOGUEADO ? (
                        <>
                            <div className="collapse navbar-collapse" id="menu">
                                <div className="navbar-nav" style={{ fontWeight: 700 }}>
                                    <a href="/" type="button" className="btn btn-dark">Inicio </a>
                                    <li><a href="/catalogo" className="nav-link px-2 text-white">Catalogo</a></li>
                                </div>
                            </div>
                            <div className="col-md-3 text-end">
                                <a href="/login" type="button" className="btn btn-sm btn-dark me-2">Ingresar</a>
                                <a href="/clientes/form" type="button" className="btn btn-sm btn-warning">Registro</a>
                            </div>
                        </>
                    ) :
                        usuario.estadologin === EstadoLogin.ADMIN ? (
                            <>
                                <div className="collapse navbar-collapse" id="menu">
                                    <div className="navbar-nav" style={{ fontWeight: 700 }}>
                                        <a href="/" type="button" className="btn btn-dark">Inicio </a>
                                        <li><a href="/catalogo" className="nav-link px-2 text-white">Catalogo</a></li>
                                        <li><a href="/productos" className="nav-link px-2 text-white">Productos</a></li>
                                        <li><a href="/clientes" className="nav-link px-2 text-white">Clientes</a></li>
                                        <li><a href="/categorias" className="nav-link px-2 text-white">Categorias</a></li>
                                        <li><a href="/pedidos" className="nav-link px-2 text-white">Pedidos</a></li>
                                        <li><a href="/envios" className="nav-link px-2 text-white">Envios</a></li>
                                    </div>
                                </div>

                                <div className="col-md-4 text-end  px-2 text-white">
                                    <a>{usuario.nombres} </a>
                                    <button onClick={cerrarSesion} type="button" className="btn btn-sm btn-dark mv-2">Cerrar sesion</button>
                                </div>
                            </>
                        ) : (
                            <>
                                <div className="collapse navbar-collapse" id="menu">
                                    <div className="navbar-nav" style={{ fontWeight: 700 }}>
                                        <a href="/" type="button" className="btn btn-dark">Inicio </a>
                                        <li><a href="/catalogo" className="nav-link px-2 text-white">Catalogo</a></li>

                                    </div>
                                </div>
                                <div className="col-md-4 text-end text-light">
                                    {usuario.nombres}
                                    <button onClick={cerrarSesion} type="button" className="btn btn-sm btn-dark ms-2 me-2">Cerrar sesion</button>
                                    <button type="button" className="btn btn-light me-2">carrito</button>
                                </div>
                            </>
                        )
                }
            </div>
        </nav>
    )
}

export default Header;