import fondo from "../../imgs/Papel.jpg";
import baner7 from "../../imgs/MilkyWay7.jpg";
import baner15 from "../../imgs/MilkyWay15.jpg";
import baner6 from "../../imgs/MilkyWay6.jpg";
import p1 from "../../imgs/chocolateria.jpg";
import p2 from "../../imgs/confiteria.jpg";
import p3 from "../../imgs/snacks.jpg";

const Bienvenida = () => {
  return (
    <main>
      <div className="wrap-block text-bg-warning bg-opacity-50 row g-0">
        <center><h1 className="center">¡Bienvenido a la magia de la alegría!</h1></center>
      </div>
      <div id="carouselExampleInterval" className="carousel slide row g-0" data-bs-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item active" data-bs-interval="10000">
            <img src={baner7} class="img-fluid" alt="..."/>
          </div>
          <div className="carousel-item" data-bs-interval="2000">
            <img src={baner15} class="img-fluid" alt="..."/>
          </div>
          <div className="carousel-item">
            <img src={baner6} class="img-fluid" alt="..."/>
          </div>
        </div>
        <div className="wrap-block text-bg-warning bg-opacity-50 row g-0">
          <center><p className="center mt-3" >MilkyWay, ¡todos los días!</p></center>
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
      <div className="container" >
        <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
          <a href="/" className="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
            <svg className="bi me-2" width="50" height="32" role="img" aria-label="Bootstrap"></svg>
          </a>
          <img src={fondo} class="img-fluid" alt="..."/>
        </div>
      </div>
      <div className="container marketing">
        <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
          <a href="/" className="d-flex align-items-center mb-1 mb-lg-0 text-white text-decoration-none">
            <svg className="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"></svg>
          </a>
          <div className="row">
            <div className="col-lg-4">
            <img src={p3} class="img-fluid" alt="..."/>

              <h2 className="fw-normal">Productos snacks</h2>
              <p>Snack, un producto más sano y equilibrado. Los snacks saludables nos pueden recargar las pilas cada mañana, a media tarde o justo.</p>
              <p><a className="btn btn-secondary" href="/catalogo">Ver mas detalles &raquo;</a></p>
            </div>
            <div className="col-lg-4">
              <img src={p2} class="img-fluid" alt="..."/>

              <h2 className="fw-normal">Productos confiteria</h2>
              <p>Productos alimenticios a base de azúcar, golosinas y caramelos, con exclusión de las elaboraciones con chocolate.</p>
              <p><a className="btn btn-secondary" href="/catalogo">Ver mas detalles &raquo;</a></p>
            </div>
            <div className="col-lg-4">
              <img src={p1} class="img-fluid" alt="..."/>

              <h2 className="fw-normal">Productos Chocolateria</h2>
              <p>Los mejores chocolates a base de avellana.</p>
              <p><a className="btn btn-secondary" href="/catalogo">Ver mas detalles &raquo;</a></p>

            </div>
          </div>
        </div>
      </div>
    </main>
  )
}

export default Bienvenida;