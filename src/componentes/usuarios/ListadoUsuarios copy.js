import { useState, useEffect } from "react";
import Estados from "../../enums/Estados";
import UsuariosServicios from "../../servicios/UsuariosServicios";

const ListadoUsuarios = () => {

    const [ listadoUsuarios, setListadoUsuarios ] = useState([]);
    const [ estado, setEstado ] = useState(Estados.CARGANDO);
    const [ criterioUsuario, setcriterioUsuario ] = useState("");

    const cargarUsuarios = async () => {
        try {
            const respuesta = await UsuariosServicios.listarUsuarios();
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoUsuarios(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }
    }

    const buscarUsuarios = async (event) => {
        event.preventDefault();
        try {
            const respuesta = await UsuariosServicios.buscarUsuarios(criterioUsuario);
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoUsuarios(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }     
    }

    useEffect(() => {
        cargarUsuarios();
    }, [])

    const cambiarcriterioUsuario = (event) => {
        setcriterioUsuario(event.target.value);
    }

    return (
        <div className="container">
            <h3 className="mt-2">Lista de usuarios</h3>
            <form action="">
                <input  type="text" value={criterioUsuario} onChange={cambiarcriterioUsuario} id="criterioUsuario" name="criterioUsuario"/>
                <button className ="btn btn-dark mx-1" id="buscar_usuario" name="buscar_usuario" onClick={buscarUsuarios}>Buscar</button>
            </form>
            <table className="table table-sm"> 
                <thead>
                    <tr>
                        <th>Nombres completos</th>
                        <th>Documento</th>
                        <th>Dirección</th>
                        <th>Telefono</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {estado === Estados.CARGANDO ? 
                            (<tr>
                                <td>
                                <div className="spinner-border text-warning" role="status">
                                    <span className="sr-only"></span>
                                </div>
                            </td>
                            </tr>) 
                        :
                        estado === Estados.ERROR ? 
                            (<tr><td>Ocurrió un error, intente más tarde</td></tr>) 
                        :
                        estado === Estados.VACIO ? 
                            (<tr><td>No hay datos</td></tr>) 
                        : 
                        listadoUsuarios.map((usuario) => (
                            <tr>
                                <td>{ usuario.nombres_usuario +" "+usuario.apellidos_usuario}</td>
                                <td>{ usuario.documento_usuario }</td>
                                <td>{ usuario.direccion_usuario }</td>
                                <td>{ usuario.telefono_usuario }</td>
                                <td>
                                <button className="btn btn-sm btn-warning me-2">Editar</button>
                                    <button className="btn btn-sm btn-secondary me-2">Eliminar</button>
                                </td>
                            </tr>
                        )) 
                    }
                </tbody>
            </table>
        </div>
    )
}

export default ListadoUsuarios;
