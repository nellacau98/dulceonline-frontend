import { useState } from "react";
import UsuariosServicios from "../../servicios/UsuariosServicios";
import { useNavigate } from "react-router-dom";

const FormUsuarios = () => {
    const navigateTo = useNavigate()

    const [nombres_usuario, setNombresUsuario] = useState("");
    const [apellidos_usuario, setApellidosUsuarios] = useState("");
    const [tipo_documento_usuarios, setTipodocumentoUsuarios] = useState("");
    const [documento_usuario, setDocumentoUsuario] = useState();
    const [direccion_usuario, setDireccionUsuarios] = useState("");
    const [ciudad_usuario, setCiudadUsuario] = useState("");
    const [telefono_usuario, setTelefonoUsuario] = useState();
    const [correo_usuario, setCorreoUsuario] = useState("");
    const [passw, setPassw] = useState("");
    const [confirm, setConfirm] = useState("");
    const [activo, setActivo] = useState(false);
    const [mensaje, setMensaje] = useState("");

    const guardarUsuario = async (event) => {
        event.preventDefault();

        if (passw === confirm) {
            try {
                const usuario = {
                    nombres_usuario: nombres_usuario,
                    apellidos_usuario: apellidos_usuario,
                    tipo_documento_usuarios: tipo_documento_usuarios,
                    documento_usuario: documento_usuario,
                    direccion_usuario: direccion_usuario,
                    ciudad_usuario: ciudad_usuario,
                    telefono_usuario: telefono_usuario,
                    correo_usuario: correo_usuario,
                    passw: passw
                }
                console.log(usuario);
                await UsuariosServicios.guardarUsuario(usuario);
                navigateTo("/");
            } catch (error) {
                setMensaje("Ocurrió un error");
            }
        }
        else {
            setMensaje("Las contraseñas no coinciden");
        }
    }

    const cambiarNombresUsuarios = (event) => {
        setNombresUsuario(event.target.value);
    }

    const cambiarApellidosUsuarios = (event) => {
        setApellidosUsuarios(event.target.value);
    }

    const cambiarTipodocumentoUsuarios = (event) => {
        setTipodocumentoUsuarios(event.target.value);
    }

    const cambiarDocumentoUsuarios = (event) => {
        setDocumentoUsuario(event.target.value);
    }

    const cambiarDireccionUsuarios = (event) => {
        setDireccionUsuarios(event.target.value);
    }

    const cambiarCiudadUsuarios = (event) => {
        setCiudadUsuario(event.target.value);
    }

    const cambiarTelefonoUsuario = (event) => {
        setTelefonoUsuario(event.target.value);
    }

    const cambiarCorreoUsuario = (event) => {
        setCorreoUsuario(event.target.value);
    }

    const cambiarPassw = (event) => {
        setPassw(event.target.value);
    }

    const cambiarConfirm = (event) => {
        setConfirm(event.target.value);
    }

    const cambiarActivo = (event) => {
        setActivo(event.target.checked);
    }

    return (
        <div className="container mt-3">
            <div>
                <h3>Registro de usuarios</h3>
                <form className="container" action="">
                    <div className="row">
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="nombres_usuario">NombresUsuarios *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarNombresUsuarios} value={nombres_usuario} name="nombres_usuario" id="nombres_usuario" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="apellidos_usuario">ApellidosUsuarios *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarApellidosUsuarios} value={apellidos_usuario} name="apellidos_usuario" id="apellidos_usuario" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="tipoidentificacion">Tipo de identificacion *</label>
                            <select className="form-select form-select-sm" type="select" onChange={cambiarTipodocumentoUsuarios} value={tipo_documento_usuarios} name="tipo_documento_usuarios" id="tipo_documento_usuarios" required >
                                <option value="NA" type="text">Seleccione tipo de documento_usuario</option>
                                <option value="CC">CC</option>
                                <option value="CE">CE</option>
                                <option value="PA">PA</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="documento_usuario">Ingrese documento_usuario *</label>
                            <input className="form-control form-control-sm" type="Number" onChange={cambiarDocumentoUsuarios} value={documento_usuario} name="documento_usuario" id="documento_usuario" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="telefono_usuario">Teléfono *</label>
                            <input className="form-control form-control-sm" type="Number" onChange={cambiarTelefonoUsuario} value={telefono_usuario} name="telefono_usuario" id="telefono_usuario" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="correo_usuario">Ciudad *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarCiudadUsuarios} value={ciudad_usuario} name="ciudad_usuario" id="ciudad_usuario" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="correo_usuario">Email *</label>
                            <input className="form-control form-control-sm" type="email" onChange={cambiarCorreoUsuario} value={correo_usuario} name="correo_usuario" id="correo_usuario" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="direccion_usuario">Ingrese dirección *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarDireccionUsuarios} value={direccion_usuario} name="direccion_usuario" id="direccion_usuario" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="passw">Contraseña *</label>
                            <input className="form-control form-control-sm" type="password" onChange={cambiarPassw} value={passw} name="passw" id="passw" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="confirm">Confirme contraseña *</label>
                            <input className="form-control form-control-sm" type="password" onChange={cambiarConfirm} value={confirm} name="confirm" id="confirm" required />
                        </div>
                        <div className="col-2">
                            <input type="checkbox" checked={activo} onChange={cambiarActivo} name="activo" id="activo" />
                            <label className="form-control-sm" htmlFor="activo">Activo</label>
                        </div>
                    </div>
                    <div className="mt-3">
                        <button onClick={guardarUsuario} className="btn btn-warning me-2">Guardar</button>
                        <a href="/" className="btn btn-dark">Cancelar</a>
                        <div id="mensaje">{mensaje}</div>
                    </div>
                </form>
            </div>
        </div>
    )
}


export default FormUsuarios;