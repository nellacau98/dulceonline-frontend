import { useState, useEffect } from "react";
import Estados from "../../enums/Estados";
import EnviosServicios from "../../servicios/EnviosServicios";

const ListadoEnvios = () => {

    const [listadoEnvios, setListadoEnvios] = useState([]);
    const [estado, setEstado] = useState(Estados.CARGANDO);
    const [criterio, setCriterio] = useState("");
    const [idBorrar, setIdBorrar] = useState("");
    const [nombreBorrar, setNombreBorrar] = useState("");



    const cargarEnvios = async () => {
        try {
            const respuesta = await EnviosServicios.listarEnvios();
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoEnvios(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }
    }

    const buscarEnvios = async (event) => {
        event.preventDefault();
        try {
            const respuesta = await EnviosServicios.buscarEnvios(criterio);
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoEnvios(respuesta.data);
                setEstado(Estados.OK);
            }
            else {
                setEstado(Estados.VACIO);
            }
        } catch (error) {
            setEstado(Estados.ERROR);
        }
    }

    useEffect(() => {
        cargarEnvios();
    }, [])

    const cambiarCriterio = (event) => {
        setCriterio(event.target.value);
    }

    const confirmarBorrado = (id, nombre) => {
        setIdBorrar(id);
        setNombreBorrar(nombre);
    }

    const borrarEnvio = async () => {
        try {
            await EnviosServicios.borrarEnvio(idBorrar);
            cargarEnvios();
        } catch (error) {

        }
    }

    return (
        <div className="container">
            <h3 className="mt-2">Lista de envios</h3>
            <form action="">
                <input type="text" value={criterio} onChange={cambiarCriterio} id="criterio" name="criterio" />
                <button className="btn btn-dark mx-1" id="buscar" name="buscar" onClick={buscarEnvios}>Buscar</button>
            </form>
            <table className="table table-sm">
                <thead>
                    <tr>
                        <th>Ciudad</th>
                        <th>Departamento</th>
                        <th>Precio</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {estado === Estados.CARGANDO ?
                        (<tr><td>
                            <div className="spinner-border text-warning" role="status">
                                <span className="sr-only"></span>
                            </div>
                        </td></tr>)
                        :
                        estado === Estados.ERROR ?
                            (<tr><td>Ocurrió un error, intente más tarde</td></tr>)
                            :
                            estado === Estados.VACIO ?
                                (<tr><td>No hay datos</td></tr>)
                                :
                                listadoEnvios.map((envio) => (
                                    <tr>
                                        <td>{envio.ciudad}</td>
                                        <td>{envio.departamento}</td>
                                        <td>{envio.precio_envio}</td>
                                        <td>
                                            <a href={"/envios/form/" + envio._id} className="btn btn-sm btn-warning me-2">Editar</a>
                                            <button onClick={() => { confirmarBorrado(envio._id, envio.ciudad) }} className="btn btn-sm btn-secondary me-2" data-bs-toggle="modal" data-bs-target="#modalBorrado">Eliminar</button>
                                        </td>
                                    </tr>
                                ))
                    }
                </tbody>
            </table>
            <a href="/envios/form" type="button" className="btn btn-dark">Crear Envio</a>

            <div className="modal fade" id="modalBorrado" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="staticBackdropLabel">Borrado de evnió</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            ¿Está seguro de borrar la ciudad de envió {nombreBorrar}?
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-warning" data-bs-dismiss="modal">Cancelar</button>
                            <button type="button" onClick={borrarEnvio} className="btn btn-secondary" data-bs-dismiss="modal">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ListadoEnvios;