import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import EnviosServicios from "../../servicios/EnviosServicios";

const FormEnvios = () => {

    const { id } = useParams();
    const navigateTo = useNavigate()

    const [ciudad, setCiudad ] = useState("");
    const [departamento, setDepartamento ] = useState("");
    const [precioenvio, setPrecio_envio ] = useState("");
    const [mensaje, setMensaje] = useState("");
    const [titulo, setTitulo] = useState("");


    const guardarEnvio = async (event) => {
        event.preventDefault();
        try {
            const envio ={
                ciudad: ciudad,
                departamento: departamento,
                precio_envio: precioenvio,
            }
            console.log(envio);
            if (id == null) {
                await EnviosServicios.guardarEnvios(envio);
                navigateTo("/envios");
            }
            else {
                await EnviosServicios.modificarProducto(id, envio);
                navigateTo("/envios");
            }
        } catch (error) {
            setMensaje("Ocurrió un errór :(")
        }
    }

    const cargarEnvio = async () => {
        try {
            if (id != null) {
                const respuesta = await EnviosServicios.buscarEnvio(id);
                if (respuesta.data != null) {
                    console.log(respuesta.data);
                    setCiudad(respuesta.data.ciudad);
                    setDepartamento(respuesta.data.departamento);
                    setPrecio_envio(respuesta.data.precio_envio);
                }
                setTitulo("Edición");
            }
            else {
                setTitulo("Registro");
            }
        } catch (error) {
            console.log("Ocurrió un error");
        }
    }

    const cancelar = () => {
        if (id != null) {
            navigateTo("/envios");
        }
        else {
            navigateTo("/");
        }
    }


useEffect(() => {
    cargarEnvio();
}, [])

    const cambiarCiudad = (event) => {
        setCiudad(event.target.value);
    }
    const cambiarDepartamento = (event) => {
        setDepartamento(event.target.value);
    }
    const cambiarPrecio_envio = (event) => {
        setPrecio_envio(event.target.value);
    }

    return(
        <div className="container mt-3">
            <div>
                <h3>{titulo} de envios</h3>
                <form className="container" action="">
                    <div className="row">
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="ciudad">Ciudad</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarCiudad} value={ciudad} name="ciudad" id="ciudad" required/>
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="departamento">Departamento</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarDepartamento} value={departamento} name="departamento" id="departamento" required/>
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="precio_envio">Precio</label>
                            <input className="form-control form-control-sm" type="number" onChange={cambiarPrecio_envio} value={precioenvio} name="precio_envio" id="precio_envio" required/>
                        </div>
                    </div>
                    <div className="mt-3">
                    <button onClick={guardarEnvio} className="btn btn-warning me-2">Guardar</button>
                    <button onClick={cancelar} href="/categorias" className="btn btn-secondary">Cancelar</button>
                    <div id="mensaje">{mensaje}</div>
                </div>
                </form>
            </div>
        </div>
    )
}


export default FormEnvios;