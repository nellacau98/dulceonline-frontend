import { useEffect } from "react";
import { useState } from "react";
import ClientesServicios from "../../servicios/ClientesServicios";
import { useNavigate, useParams } from "react-router-dom";

const FormClientes = () => {
    const { id } = useParams();
    const navigateTo = useNavigate()

    const [nombres, setNombres] = useState("");
    const [apellidos, setApellidos] = useState("");
    const [tipo_documento, setTipodocumento] = useState("");
    const [documento, setDocumento] = useState();
    const [direccion, setDireccion] = useState("");
    const [ciudad, setCiudad] = useState("");
    const [telefono, setTelefono] = useState();
    const [email, setEmail] = useState("");
    const [passw, setPassw] = useState("");
    const [es_admin, setAdmin] = useState("");
    const [confirm, setConfirm] = useState("");
    const [mensaje, setMensaje] = useState("");
    const [titulo, setTitulo] = useState("");

    const guardarCliente = async (event) => {
        event.preventDefault();

        if (passw === confirm) {
            try {
                const cliente = {
                    nombres: nombres,
                    apellidos: apellidos,
                    tipo_documento: tipo_documento,
                    documento: documento,
                    direccion: direccion,
                    ciudad: ciudad,
                    telefono: telefono,
                    email: email,
                    passw: passw,
                    es_admin: es_admin
                }
                console.log(cliente);
                if (id == null) {
                    await ClientesServicios.guardarCliente(cliente);
                    navigateTo("/");
                }
                else {
                    await ClientesServicios.modificarCliente(id, cliente);
                    navigateTo("/clientes");
                }
            } catch (error) {
                setMensaje("Ocurrió un error");
            }
        }
        else {
            setMensaje("Las contraseñas no coinciden");
        }
    }


    const cargarCliente = async () => {
        try {
            if (id != null) {
                const respuesta = await ClientesServicios.buscarCliente(id);
                if (respuesta.data != null) {
                    console.log(respuesta.data);
                    setNombres(respuesta.data.nombres);
                    setApellidos(respuesta.data.apellidos);
                    setTipodocumento(respuesta.data.tipo_documento);
                    setDocumento(respuesta.data.documento);
                    setDireccion(respuesta.data.direccion);
                    setCiudad(respuesta.data.ciudad);
                    setTelefono(respuesta.data.telefono);
                    setEmail(respuesta.data.email);
                    setPassw(respuesta.data.passw);
                    setConfirm(respuesta.data.passw);
                    setAdmin(respuesta.data.es_admin)
                }
                setTitulo("Edición");
            }
            else {
                setTitulo("Registro");
            }
        } catch (error) {
            console.log("Ocurrió un error");
        }
    }


    const cancelar = () => {
        if (id != null) {
            navigateTo("/clientes");
        }
        else {
            navigateTo("/");
        }
    }

    useEffect(() => {
        cargarCliente();
    }, [])

    const cambiarNombres = (event) => {
        setNombres(event.target.value);
    }

    const cambiarApellidos = (event) => {
        setApellidos(event.target.value);
    }

    const cambiarTipodocumento = (event) => {
        setTipodocumento(event.target.value);
    }

    const cambiarDocumento = (event) => {
        setDocumento(event.target.value);
    }

    const cambiarDireccion = (event) => {
        setDireccion(event.target.value);
    }

    const cambiarCiudad = (event) => {
        setCiudad(event.target.value);
    }

    const cambiarTelefono = (event) => {
        setTelefono(event.target.value);
    }

    const cambiarEmail = (event) => {
        setEmail(event.target.value);
    }

    const cambiarPassw = (event) => {
        setPassw(event.target.value);
    }

    const cambiarConfirm = (event) => {
        setConfirm(event.target.value);
    }

    const cambiarAdmin = (event) => {
        setAdmin(event.target.value);
    }

    return (
        <div className="container mt-3">
            <div>
                <h3>{titulo} de clientes</h3>
                <form className="container" action="">
                    <div className="row">
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="nombres">Nombres *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarNombres} value={nombres} name="nombres" id="nombres" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="apellidos">Apellidos *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarApellidos} value={apellidos} name="apellidos" id="apellidos" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="tipoidentificacion">Tipo de identificacion *</label>
                            <select className="form-select form-select-sm" type="select" onChange={cambiarTipodocumento} value={tipo_documento} name="tipo_documento" id="tipo_documento" required >
                                <option value="NA" type="text">Seleccione tipo de documento</option>
                                <option value="CC">CC</option>
                                <option value="CE">CE</option>
                                <option value="PA">PA</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="documento">Ingrese documento *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarDocumento} readOnly={id != null ? true : false} value={documento} name="documento" id="documento" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="telefono">Teléfono *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarTelefono} value={telefono} name="telefono" id="telefono" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="ciudad">Ciudad *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarCiudad} value={ciudad} name="ciudad" id="ciudad" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="email">Email *</label>
                            <input className="form-control form-control-sm" type="email" onChange={cambiarEmail} value={email} name="email" id="email" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="direccion">Ingrese dirección *</label>
                            <input className="form-control form-control-sm" type="text" onChange={cambiarDireccion} value={direccion} name="direccion" id="direccion" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="passw">Contraseña *</label>
                            <input className="form-control form-control-sm" type="password" onChange={cambiarPassw} value={passw} name="passw" id="passw" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="confirm">Confirme contraseña *</label>
                            <input className="form-control form-control-sm" type="password" onChange={cambiarConfirm} value={confirm} name="confirm" id="confirm" required />
                        </div>
                        <div className="col-4">
                            <label className="form-control-sm" htmlFor="es_admin">¿Es usuario administrador? *</label>
                            <select className="form-select form-select-sm" type="select" onChange={cambiarAdmin} value={es_admin} name="es_admin" id="es_admin" required >
                                <option value="NA" type="text">Seleccione tipo de usuario</option>
                                <option value="true">Administrador</option>
                                <option value="false">Cliente</option>
                            </select>
                        </div>
                    </div>
                    <div className="mt-3">
                        <button onClick={guardarCliente} className="btn btn-sm btn-warning me-2">Guardar</button>
                        <button onClick={cancelar} className="btn btn-sm btn-secondary me-2">Cancelar</button>
                        <div id="mensaje">{mensaje}</div>
                    </div>
                </form>
            </div>
        </div>
    )

}

export default FormClientes;