import axios from "axios";
import baseURL from "../config";

const LoginServicios = {}
const URL = baseURL + "login/";

LoginServicios.login = (credenciales) => {
    return axios.post(URL, credenciales);
}

export default LoginServicios;