import axios from "axios";
import baseURL from "../config";

const UsuariosServicios = {};
const URL = baseURL + "usuarios/";

UsuariosServicios.listarUsuarios = () => {
    return axios.get(URL);
}

UsuariosServicios.buscarUsuarios = (criterio) => {
    return axios.get(URL+ "?q="+ criterio);
}

UsuariosServicios.guardarUsuario = (usuario) => {
    return axios.post(URL, usuario);
}
export default UsuariosServicios;
