import axios from "axios";
import baseURL from "../config";

const ClientesServicios = {};
const URL = baseURL + "categorias/";

const CategoriasServicios = {};

CategoriasServicios.listarCategorias = () => {
    return axios.get(URL);
} 

CategoriasServicios.buscarCategorias = (criterio) => {
    return axios.get(URL+ "?q="+criterio);
}

CategoriasServicios.buscarCategoria = (id) => {
    return axios.get(URL+id);
}

CategoriasServicios.guardarCategoria = (categoria) => {
    return axios.post(URL, categoria);
}

CategoriasServicios.modificarCategoria = (id, categoria) => {
    return axios.put(URL+id, categoria);
}

CategoriasServicios.borrarCategoria = (id) => {
    return axios.delete(URL+id);
}

export default CategoriasServicios;