import axios from "axios";
import baseURL from "../config";

const ClientesServicios = {};
const URL = baseURL + "clientes/";

ClientesServicios.listarClientes = () => {
    return axios.get(URL);
}

ClientesServicios.buscarClientes = (criterio) => {
    return axios.get(URL + "?q=" + criterio);
}

ClientesServicios.guardarCliente = (cliente) => {
    return axios.post(URL, cliente);
}

ClientesServicios.buscarCliente = (id) => {
    return axios.get(URL+id);
}

ClientesServicios.modificarCliente = (id,cliente) => {
    return axios.put(URL+id,cliente);
}

ClientesServicios.borrarCliente = (id) => {
    return axios.delete(URL+id);
}


export default ClientesServicios;
