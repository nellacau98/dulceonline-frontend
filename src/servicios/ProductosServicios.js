import axios from "axios";
import baseURL from "../config";

const ProductosServicios = {};
const URL = baseURL + "Productos/";

ProductosServicios.listarProductos = () => {
    return axios.get(URL);
} 

ProductosServicios.buscarProductos = (criterio) => {
    return axios.get(URL + "?q="+criterio);
}

ProductosServicios.buscarProducto = (id) => {
    return axios.get(URL+id);
}

ProductosServicios.guardarProducto = (categoria) => {
    return axios.post(URL, categoria);
}

ProductosServicios.modificarProducto = (id, categoria) => {
    return axios.put(URL+id, categoria);
}

ProductosServicios.borrarProducto = (id) => {
    return axios.delete(URL+id);
}

export default ProductosServicios;