import axios from "axios";
import baseURL from "../config";

const PedidosServicios = {};
const URL = baseURL + "pedidos/";


PedidosServicios.listarPedidos = () => {
    return axios.get(URL);
}

PedidosServicios.buscarPedidos = (criterio) => {
    return axios.get(URL + "?q="+criterio);
}

PedidosServicios.guardarPedido = (pedido) => {
    return axios.post(URL, pedido);
}

PedidosServicios.buscarPedido = (id) => {
    return axios.get(URL+id);
}

PedidosServicios.modificarPedido = (id,pedido) => {
    return axios.put(URL +id, pedido);
}

PedidosServicios.borrarPedido = (id) => {
    return axios.delete(URL+id);
}


export default PedidosServicios;