import axios from "axios";
import baseURL from "../config";

const EnviosServicios = {};
const URL = baseURL + "envios/";

EnviosServicios.listarEnvios = () => {
    return axios.get(URL);
}

EnviosServicios.buscarEnvios = (criterio) => {
    return axios.get(URL+ "?q="+criterio);
}

EnviosServicios.buscarEnvio = (id) => {
    return axios.get(URL+id);
}

EnviosServicios.guardarEnvios =(envio) =>{
    return axios.post(URL, envio);
}

EnviosServicios.modificarProducto = (id, envio) => {
    return axios.put(URL+id, envio)
}

EnviosServicios.borrarEnvio = (id) => {
    return axios.delete(URL+id);
}

export default EnviosServicios;