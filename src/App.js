import { BrowserRouter, Route, Routes } from "react-router-dom";
import ListadoClientes from "./componentes/clientes/ListadoClientes";
import FormClientes from "./componentes/clientes/FormClientes";
import ListadoProductos from "./componentes/productos/ListadoProductos";
import FormProductos from "./componentes/productos/FormProductos";
import FormCategorias from "./componentes/categorias/FormCategorias";
import ListadoCategorias from "./componentes/categorias/ListadoCategorias";
import ListadoUsuarios from "./componentes/usuarios/ListadoUsuarios";
import ListadoEnvios from "./componentes/envios/ListadoEnvios";
import FormEnvios from "./componentes/envios/FormEnvios";
import FormUsuarios from "./componentes/usuarios/FormUsuarios";
import ListadoPedidos from "./componentes/pedidos/ListadoPedidos";
import FormPedidos from "./componentes/pedidos/FormPedidos";
import Header from "./componentes/general/Header";
import Bienvenida from "./componentes/general/Bienvenida";
import Login from "./componentes/general/Login";
import { ContextoUsuario } from "./componentes/general/ContextoUsuario";
import { useState } from "react";
import Dashboard from "./componentes/general/Dashboard";
import Catalogo from "./componentes/productos/Catalogo";

const App = () => {
  const [usuario, setUsuario] = useState({nombres: "", estadologin: 0});
  return (
      <BrowserRouter>
      <ContextoUsuario.Provider value={{usuario, setUsuario}}>
      <Header />
        <Routes>
          <Route path="/" element={<Bienvenida />} exact></Route>
          <Route path="/login" element={<Login />} exact />
          <Route path="/dashboard" element={<Dashboard />} exact />
          <Route path="/clientes" element={<ListadoClientes />} exact></Route>
          <Route path="/clientes/form" element={<FormClientes />} exact></Route>
          <Route path="/clientes/form/:id" element={<FormClientes />} exact></Route>
          <Route path="/catalogo" element={<Catalogo />} exact></Route>
          <Route path="/productos" element={<ListadoProductos />} exact></Route>
          <Route path="/productos/form" element={<FormProductos />} exact></Route>
          <Route path="/productos/form/:id" element={<FormProductos />} exact></Route>
          <Route path="/categorias" element={<ListadoCategorias />} exact></Route>
          <Route path="/categorias/form" element={<FormCategorias />} exact></Route>
          <Route path="/categorias/form/:id" element={<FormCategorias />} exact></Route>
          <Route path="/usuarios" element={<ListadoUsuarios />} exact></Route>
          <Route path="/usuarios/form" element={<FormUsuarios />} exact></Route>
          <Route path="/envios" element={<ListadoEnvios />} exact></Route>
          <Route path="/envios/form" element={<FormEnvios />} exact></Route>
          <Route path="/envios/form/:id" element={<FormEnvios />} exact></Route>
          <Route path="/pedidos" element={<ListadoPedidos />} exact></Route>
          <Route path="/pedidos/form" element={<FormPedidos />} exact></Route>
          <Route path="/pedidos/form/:id" element={<FormPedidos />} exact></Route>
        </Routes>
        </ContextoUsuario.Provider>
      </BrowserRouter>
  
  );
}

export default App;
